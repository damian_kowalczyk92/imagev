class AppConfig(object):

    VALID_IMAGE_EXTENSIONS = [
        ".jpg",
        ".jpeg",
        ".png",
    ]

config = AppConfig()
