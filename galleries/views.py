import json
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponseRedirect
from django.urls.base import reverse
from django.http import JsonResponse
from django.views.generic import CreateView, ListView, UpdateView, DetailView, DeleteView, View

from galleries.forms import AddImageForm
from galleries.models import GalleryImage
from galleries.messages import messages as msg_gallery
from vision.vision_api import VisionApi


class AddImageView(LoginRequiredMixin, CreateView):
    template_name = 'galleries/image_add.html'
    form_class = AddImageForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.profile = self.request.user.profile
        obj.save()
        messages.success(self.request, msg_gallery.ADD_NEW_IMAGE_SUCCESSFUL)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('profile:dashboard')


class EditImageView(LoginRequiredMixin, UpdateView):
    template_name = 'galleries/image_edit.html'
    form_class = AddImageForm
    model = GalleryImage

    def form_valid(self, form):
        messages.success(self.request, msg_gallery.ADD_NEW_IMAGE_SUCCESSFUL)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('gallery:list-image')


class ListImageView(LoginRequiredMixin, ListView):
    template_name = 'galleries/image_list.html'
    model = GalleryImage


class DetailImageView(LoginRequiredMixin, DetailView):
    template_name = 'galleries/image_detail.html'
    model = GalleryImage
    context_object_name = 'image'


class DeleteImageView(LoginRequiredMixin, DeleteView):
    model = GalleryImage
    template_name = 'galleries/confirm_delete_image.html'

    def get_success_url(self):
        return reverse('gallery:list-image')


class UpdateVisionImageDataView(LoginRequiredMixin, View):

    http_method_names = ['post']

    def post(self, *args, **kwargs):
        image_pk = kwargs.get('pk', None)
        try:
            image_obj = GalleryImage.objects.get(pk=image_pk)
            vision = VisionApi()
            img_vision = vision.open_image_file(image_obj.image.url)
            image_obj.labels_vison = vision.get_labels_detection(img_vision)
            image_obj.face_detect_vison = vision.get_face_detection(img_vision)
            image_obj.save()
            data = {
                'success': True,
                'message': u'Aktualizacja danych zakończona pomyślnie'
            }
        except GalleryImage.DoesNotExist:
            data = {
                'success': False,
                'message': u'Aktualizacja danych nie powiodła się'
            }
        return JsonResponse(data)


class VisionFaceDetectDataView(LoginRequiredMixin, View):

    http_method_names = ['post']

    def post(self, *args, **kwargs):
        image_pk = kwargs.get('pk', None)
        try:
            image_obj = GalleryImage.objects.get(pk=image_pk)

            data = {
                'success': True,
                'face_data': json.loads(image_obj.face_detect_vison),
                'labels_data': json.loads(image_obj.labels_vison)
            }
        except GalleryImage.DoesNotExist:
            data = {
                'success': False,
            }
        return JsonResponse(data)
