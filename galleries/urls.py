from django.urls import path
from galleries.views import AddImageView, ListImageView, EditImageView, DetailImageView, DeleteImageView, \
    UpdateVisionImageDataView, VisionFaceDetectDataView

app_name = 'galleries'

urlpatterns = [
    path('', ListImageView.as_view(), name='list-image'),
    path('add/', AddImageView.as_view(), name='add-image'),
    path('detail/<int:pk>', DetailImageView.as_view(), name='detail-image'),
    path('edit/<int:pk>', EditImageView.as_view(), name='edit-image'),
    path('delete/<int:pk>', DeleteImageView.as_view(), name='delete-image'),
    path('update-vision/<int:pk>', UpdateVisionImageDataView.as_view(), name='update-vision-image'),
    path('vision-face/<int:pk>', VisionFaceDetectDataView.as_view(), name='vision-face-detect-data'),
]
