from django.utils.translation import ugettext_lazy as _


class Messages(object):

    IMAGE = _(u'Zdjęcie')
    IMAGE_TITLE = _(u'Tytuł zdjęcia')
    IMAGE_DESCRIPTION = _(u'Opis zdjęcia')
    IMAGE_TIMESTAMP = _(u'Dodano')
    HEIGHT = _(u'Wysokość')
    WIDTH = _(u'Szerokość')

    ADD_NEW_IMAGE_SUCCESSFUL = _(u'Dodano nowe zdjęcie')
    EDIT_IMAGE_SUCCESSFUL = _(u'Edycja zdjęcie zakończona pomyślnie')
    IMAGE_FIELD_NOT_EMPTY = _(u'Pole nie może być puste!')
    IMAGE_FILE_EXTENSION_DONT_MATCH = _(u'Niepoprawne rozszerzenie zdjęcia!')

messages = Messages()
