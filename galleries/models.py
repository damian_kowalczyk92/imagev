from django.db import models
from django.urls.base import reverse
from django.contrib.postgres.fields import JSONField
from sorl.thumbnail import ImageField

from profiles.models import Profile
from galleries.messages import messages as msg_gallery


def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.profile.id, filename)


class GalleryImage(models.Model):

    profile = models.ForeignKey(
        Profile,
        related_name='images',
        on_delete=models.CASCADE
    )
    title = models.CharField(
        max_length=100,
        default='',
        verbose_name=msg_gallery.IMAGE_TITLE
    )
    description = models.TextField(
        default='',
        verbose_name=msg_gallery.IMAGE_DESCRIPTION
    )
    image = ImageField(
        upload_to=user_directory_path,
        null=True,
        width_field="width_field",
        height_field="height_field",
        verbose_name=msg_gallery.IMAGE
    )
    height_field = models.IntegerField(
        default=0,
        verbose_name=msg_gallery.HEIGHT
    )
    width_field = models.IntegerField(
        default=0,
        verbose_name=msg_gallery.WIDTH
    )
    timestamp = models.DateTimeField(
        auto_now=False,
        auto_now_add=True,
        verbose_name=msg_gallery.IMAGE_TIMESTAMP
    )
    labels_vison = JSONField(
        null=True,
        default=None,
    )
    face_detect_vison = JSONField(
        null=True,
        default=None,
    )

    class Meta:
        ordering = ['-timestamp']

    def __str__(self):
        return self.title

    def get_detail_url(self):
        return reverse('gallery:detail-image', kwargs={'pk': self.pk})

    def get_edit_url(self):
        return reverse('gallery:edit-image', kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse('gallery:delete-image', kwargs={'pk': self.pk})

    def get_update_vision_url(self):
        return reverse('gallery:update-vision-image', kwargs={'pk': self.pk})

    def get_vision_face_detect_url(self):
        return reverse('gallery:vision-face-detect-data', kwargs={'pk': self.pk})
