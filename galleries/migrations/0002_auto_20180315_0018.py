# Generated by Django 2.0.3 on 2018-03-14 23:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('galleries', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='galleryimage',
            name='height_field',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='width_field',
            field=models.IntegerField(default=0),
        ),
    ]
