from django.db.models.signals import post_save
from django.dispatch import receiver

from vision.vision_api import VisionApi
from galleries.models import GalleryImage


@receiver(post_save, sender=GalleryImage)
def get_vision_data_img(sender, instance, created, **kwargs):
    if created and instance.image:
        vision = VisionApi()
        image = vision.open_image_file(instance.image.url)
        instance.labels_vison = vision.get_labels_detection(image)
        instance.face_detect_vison = vision.get_face_detection(image)
        instance.save()
