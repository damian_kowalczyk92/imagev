from django import forms
from PIL import Image
from io import BytesIO

from galleries.models import GalleryImage
from galleries.messages import messages as msg_galleries
from galleries.config import config as conf_galleries
from utils.files import valid_file_extension
from utils.forms import BootstrapForm


class AddImageForm(BootstrapForm, forms.ModelForm):

    class Meta:
        model = GalleryImage
        fields = ('title', 'image', 'width_field', 'height_field', 'description')

    def clean_image(self):
        image = self.cleaned_data.get('image')
        if image is None:
            raise forms.ValidationError(msg_galleries.IMAGE_FIELD_NOT_EMPTY)
        if not valid_file_extension(image.name, conf_galleries.VALID_IMAGE_EXTENSIONS):
            raise forms.ValidationError(msg_galleries.IMAGE_FILE_EXTENSION_DONT_MATCH)
        return image

    def save(self, *args, **kwargs):
        if self.instance.image:
            # resize saved image to standard size for all
            image_file = BytesIO(self.instance.image.file.read())
            image = Image.open(image_file)
            image.thumbnail((600, 600), Image.ANTIALIAS)
            image_file = BytesIO()
            image.save(image_file, 'JPEG')
            self.instance.image.file = image_file
        return super(AddImageForm, self).save(*args, **kwargs)
