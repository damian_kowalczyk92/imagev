from django.contrib import admin
from galleries.models import GalleryImage

admin.site.register(GalleryImage)
