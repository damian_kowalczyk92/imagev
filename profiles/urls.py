from django.urls import path
from profiles.views import ProfileDashboardView

app_name = 'profiles'

urlpatterns = [
    path('', ProfileDashboardView.as_view(), name='dashboard'),
]
