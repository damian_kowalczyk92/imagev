# Generated by Django 2.0.3 on 2018-03-13 19:37

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('city', models.CharField(default='', max_length=50, verbose_name='Miasto')),
                ('street', models.CharField(default='', max_length=50, verbose_name='Ulica')),
                ('zip_code', models.CharField(default='', max_length=6, verbose_name='Kod pocztowy')),
                ('phone', models.CharField(default='', max_length=20, verbose_name='Telefon')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
