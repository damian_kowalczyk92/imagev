from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class ProfileDashboardView(LoginRequiredMixin, TemplateView):
    template_name = 'profiles/dashboard.html'
