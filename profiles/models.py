from django.contrib.auth.models import User
from django.db import models
from profiles.messages import messages as msg_profile


class Profile(User):

    city = models.CharField(
        max_length=50,
        default='',
        verbose_name=msg_profile.CITY
    )
    street = models.CharField(
        max_length=50,
        default='',
        verbose_name=msg_profile.STREET
    )
    zip_code = models.CharField(
        max_length=6,
        default='',
        verbose_name=msg_profile.ZIP_CODE
    )
    phone = models.CharField(
        max_length=20,
        default='',
        verbose_name=msg_profile.PHONE
    )

    def __str__(self):
        return self.first_name + ' ' + self.last_name

