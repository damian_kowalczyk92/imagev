from django.utils.translation import ugettext_lazy as _


class Messages(object):

    COMPANY_NAME = _(u'Nazwa firmy')
    CITY = _(u'Miasto')
    STREET = _(u'Ulica')
    ZIP_CODE = _(u'Kod pocztowy')
    PHONE = _(u'Telefon')

messages = Messages()
