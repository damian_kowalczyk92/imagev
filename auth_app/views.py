from django.contrib import messages
from django.shortcuts import render
from django.contrib.auth import authenticate, logout, login
from django.http import HttpResponseRedirect, Http404
from django.urls.base import reverse
from django.views.generic import TemplateView, RedirectView, CreateView
from auth_app.forms import LoginForm, RegistrationProfileForm
from auth_app.messages import messages as auth_messages
from utils.auth import get_target


class LoginUserView(TemplateView):
    form_class = LoginForm
    template_name = "auth/login.html"

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'user_login_form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        target = request.GET.get('next', None)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    target = request.GET.get('next', None)
                    if target is None:
                        target = get_target(user)
                        if target is None:
                            logout(request)
                            raise Http404
                    return HttpResponseRedirect(target)
                else:
                    # user not active
                    return render(request, self.template_name,
                                  {'user_login_form': form, 'messages': [auth_messages.USER_NOT_ACTIVE]})
            else:
                # user doesn't exist
                return render(request, self.template_name, {'user_login_form': form,
                                                            'messages': [auth_messages.USER_NOT_EXIST]})

        return render(request, self.template_name, {'user_login_form': form})


class LogoutView(RedirectView):
    url = '/'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class RegistrationProfileView(CreateView):
    template_name = 'auth/registration.html'
    form_class = RegistrationProfileForm

    def get_success_url(self):
        return reverse('home-page')

    def form_valid(self, form, *args, **kwargs):
        messages.success(self.request, auth_messages.INFO_ACCOUNT_CREATED)
        return super(RegistrationProfileView, self).form_valid(form, *args, **kwargs)
