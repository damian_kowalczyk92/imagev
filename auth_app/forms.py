from django import forms
from django.contrib.auth.models import User
from auth_app.messages import messages as auth_messages
from profiles.models import Profile
from utils.forms import BootstrapForm


class LoginForm(BootstrapForm, forms.Form):
    username = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={'placeholder': auth_messages.USERNAME}),
        label=''
    )
    password = forms.CharField(
        max_length=100,
        widget=forms.PasswordInput(attrs={'placeholder': auth_messages.PASSWORD}),
        label=''
    )


class RegistrationProfileForm(BootstrapForm, forms.ModelForm):

    password2 = forms.CharField(
        max_length=128,
        label=auth_messages.CONFIRM_PASSWORD,
        widget=forms.PasswordInput
    )

    class Meta:
        model = Profile
        fields = ('username', 'email', 'password', 'password2', 'first_name', 'last_name', 'phone')
        widgets = {
            'password': forms.PasswordInput(),
        }

    def __init__(self, *args, **kwargs):
        super(RegistrationProfileForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].required = True
            self.fields[field].widget.attrs.update({'placeholder': self.fields[field].label})
            self.fields[field].label = ''
        self.fields['username'].help_text = ''

    def clean_email(self):
        email = self.cleaned_data.get('email')
        try:
            User.objects.get(email__iexact=email)
            raise forms.ValidationError(auth_messages.EMAIL_ADDRESS_IS_BUSY)
        except User.DoesNotExist:
            return email

    def clean(self):
        cleaned_data = self.cleaned_data
        password = cleaned_data.get('password')
        password2 = cleaned_data.get('password2')
        if password and password2:
            if password != password2:
                self.add_error('password', auth_messages.PASSWORD_NOT_THE_SAME)
                self.add_error('password2', auth_messages.PASSWORD_NOT_THE_SAME)
        return cleaned_data

    def save(self, *args, **kwargs):
        user = super(RegistrationProfileForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        return super(RegistrationProfileForm, self).save(*args, **kwargs)
