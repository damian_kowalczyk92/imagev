from django.utils.translation import ugettext_lazy as _


class Messages(object):

    USERNAME = _(u'Login')
    PASSWORD = _(u'Hasło')
    CONFIRM_PASSWORD = _(u'Potwierdź hasło')

    USER_NOT_ACTIVE = _(u'Użytkownik nie jest aktywny')
    USER_NOT_EXIST = _(u'Użytkownik nie istnieje')

    INFO_ACCOUNT_CREATED = _(u'Rejestracja zakończona powodzeniem')
    EMAIL_ADDRESS_IS_BUSY = _(u'Adres e-mail jest już zajęty')
    PASSWORD_NOT_THE_SAME = _(u'Hasła nie są identyczne')

messages = Messages()
