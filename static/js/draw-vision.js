function drawImage(){
    var img = document.getElementById("image-vision")
    var canvas = document.getElementById('img-canvas')
    var ctx = canvas.getContext('2d');
    canvas.width = img.width
    canvas.height = img.height
    ctx.drawImage(img, 0, 0)
    ctx.strokeStyle = 'rgba(0,255,0,0.8)';
    ctx.fillStyle = 'rgba(0,255,0,0.8)';
    ctx.lineWidth = '2';
    return ctx
}

function highlightFaces(context, data){
    if (data.faceAnnotations){
        $.each(data.faceAnnotations, function(index, value){
            context.beginPath();
            let origX = 0;
            let origY = 0;
            let fdorigX = 0;
            let fdorigY = 0;
            let num_face = index+=1
            let text_pos_x = value.fdBoundingPoly.vertices[0].x + 5
            let text_pos_y = value.fdBoundingPoly.vertices[0].y - 2
            $.each(value.boundingPoly.vertices, function(index, value){
                if (index === 0) {
                    origX = value.x;
                    origY = value.y;
                }
                context.lineTo(value.x, value.y);
            })
            context.lineTo(origX, origY);
            context.stroke();

            context.beginPath();
            context.font = "normal 15px Arial";
            context.fillText('Face ' + num_face , text_pos_x, text_pos_y);
            $.each(value.fdBoundingPoly.vertices, function(index, value){
                if (index === 0) {
                    fdorigX = value.x;
                    fdorigY = value.y;
                }
                context.lineTo(value.x, value.y);
            })
            context.lineTo(fdorigX, fdorigY);
            context.stroke();
        })

    }
}

function highlightLandmarks(context, data){
    if (data.faceAnnotations){
        $.each(data.faceAnnotations, function(index, face){
            $.each(face.landmarks, function(index, landmarks){
                context.beginPath();
                context.arc(landmarks.position.x, landmarks.position.y, 1, 0, 2*Math.PI);
                context.stroke();
            })
        })

    }
}

function createRateElement(){
   let div = document.createElement("div")
   div.classList.add("rate")
   return div
}

function generateRateLikeliHood(elements, bar){
    for(i=0; i < elements; i++){
        bar.append(createRateElement())
    }
}

function showFaceData(data){
    let likelihood_name = ['UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY', 'POSSIBLE', 'LIKELY', 'VERY_LIKELY']
    let data_wrapper = $('#results')

    $.each(data.faceAnnotations, function(index, face){
        let num_face = index += 1
        let detectionConfidence = Math.round(face.detectionConfidence * 100)
        let panAngle = Math.round(face.panAngle)
        let rollAngle = Math.round(face.rollAngle)
        let tiltAngle = Math.round(face.tiltAngle)
        let $element = $(`
        <div class='mt-3 mb-3'>
            <div class='data-box'>
                <div class='header'>Face: ${num_face}</div>
                <div class='data_row'>
                    <div class='label'>Joy:</div>
                    <div class='bar'></div>
                    <div class='description'>${face.joyLikelihood}</div>
                </div>
                <div class='data_row'>
                    <div class='label'>Sorrow:</div>
                    <div class='bar'></div>
                    <div class='description'>${face.sorrowLikelihood}</div>
                </div>
                <div class='data_row'>
                    <div class='label'>Anger:</div>
                    <div class='bar'></div>
                    <div class='description'>${face.angerLikelihood}</div>
                </div>
                <div class='data_row'>
                    <div class='label'>Surprise:</div>
                    <div class='bar'></div>
                    <div class='description'>${face.surpriseLikelihood}</div>
                </div>
                <div class='data_row'>
                    <div class='label'>Exposed:</div>
                    <div class='bar'></div>
                    <div class='description'>${face.underExposedLikelihood}</div>
                </div>
                <div class='data_row'>
                    <div class='label'>Blurred:</div>
                    <div class='bar'></div>
                    <div class='description'>${face.blurredLikelihood}</div>
                </div>
                <div class='data_row'>
                    <div class='label'>Headwear:</div>
                    <div class='bar'></div>
                    <div class='description'>${face.headwearLikelihood}</div>
                </div>
                <div class="angles mt-2">
                    <span class="roll key">Roll:</span><span class="roll value">${rollAngle}&deg;</span>
                    <span class="tilt key">Tilt:</span><span class="tilt value">${tiltAngle}&deg;</span>
                    <span class="pan key">Pan:</span><span class="pan value">${panAngle}&deg;</span>
                </div>
                <div class="confidence mt-2">
                    <div class="conf-name text-left">Confidence:</div>
                    <div class="conf-score text-right">${detectionConfidence}%</div>
                    <div class="conf-meter">
                        <div class="conf-bar" style="width: ${detectionConfidence}%"></div>
                    </div>
                </div>
            </div>
        </div>`);
        data_wrapper.append($element);
    })
    let $data_row = $('.data_row')
    let $data_description = $data_row.find('.description')
    $.each($data_description, function(index, des){
        let $bar = $data_row[index].children[1]
        generateRateLikeliHood(likelihood_name.indexOf(des.innerText), $bar)
    })
}

function showLabelData(data){
    let data_wrapper = $('#results')
    $.each(data.labelAnnotations, function(index, label){
        let labelDescription = label.description[0].toUpperCase() + label.description.slice(1);
        let labelScore = Math.round(label.score * 100)
        $label = $(`
            <div class="data_row mt-2 mb-2">
                <div class="lab-name text-left">
                    <a target="_blank" title="Search for ${labelDescription} on Google" href="https://www.google.com/search?q=${labelDescription}">${labelDescription}</a>
                </div>
                <div class="lab-score text-right">${labelScore}%</div>
                <div class="lab-meter">
                    <div class="lab-bar" style="width: ${labelScore}%;"></div>
                </div>
            </div>
        `)
        data_wrapper.append($label)
    });
}

function showNoDataMsgNotify(){
    let data_wrapper = $('#results')
    $msg = $(`<div class="data_row mt-2 mb-2">No data available</div>`)
    data_wrapper.append($msg)
}

$( document ).ready(function(){
    let results = $('#results')
    context = drawImage()
    getFaceDetectData(context)

    $('.face-trigger').click(function(){
        results.empty()
        getFaceDetectData(context)
    });

    $('.label-trigger').click(function(){
        drawImage()
        results.empty()
        getLabelsData()
    });
})
