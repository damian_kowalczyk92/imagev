function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function createMessagesNotify(success, message){
        const bodyEl = document.querySelector("body")
        let ulEl = document.createElement("ul")
        let liEl = document.createElement("li")
        let msgContentEl = document.createElement("div")
        ulEl.classList.add("messages")
        if (success) {
            liEl.classList.add("success")
        } else {
            liEl.classList.add("error")
        }
        msgContentEl.classList.add("msg-content")
        msgContentEl.innerText = message
        liEl.appendChild(msgContentEl)
        ulEl.appendChild(liEl)
        bodyEl.appendChild(ulEl)
    }

function hideNotify(){
    $('.messages').delay(2000).fadeOut();
}


$(document).ready(function(){
    $('.btn-success').click(function(){
        $(this).append($('<div class="loader"></div>'))
        url = $(this).attr('data-vision');
        $.ajax({
            headers: {
                "X-CSRFToken": getCookie("csrftoken")
            },
            url         : url,
            type        : "post",
            contentType : 'aplication/json',
            dataType    : 'json'
        }).done(function(response) {
            createMessagesNotify(response.success, response.message)
            hideNotify()
            $('.loader').remove()
        });
    });
});
