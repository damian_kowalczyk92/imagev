
def valid_file_extension(file_name, extension_list):
    return any([file_name.endswith(e) for e in extension_list])
