from django.urls import reverse


def get_target(user):
    target = reverse('home-page')
    if user.is_superuser:
        target = reverse('admin:index')
    elif hasattr(user, 'profile'):
        target = reverse('profile:dashboard')
    return target
