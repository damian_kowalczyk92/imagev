import io
from google.cloud import vision
from google.protobuf.json_format import MessageToJson
from django.conf import settings


class VisionApi(object):

    def __init__(self):
        self.client = vision.ImageAnnotatorClient()

    def open_image_file(self, image_file_name):
        path_to_img = settings.BASE_DIR + image_file_name
        with io.open(path_to_img, 'rb') as image_file:
            content = image_file.read()

        image = vision.types.Image(content=content)
        return image

    def get_labels_detection(self, image):
        response = self.client.label_detection(image=image)
        serialized = MessageToJson(response)
        return serialized

    def get_face_detection(self, image):
        response = self.client.face_detection(image=image)
        serialized = MessageToJson(response)
        return serialized
