Steps to run application:
1. Create virtual environment with python3 to use for project and install all dependencies from requirements.txt file.
2. Create database PostgreSQL on local machine and add own settings to connection with db. Example of this settings You find in setting.py file.
3. Run migration db by python3 manage.py migrate
4. Add to Your system environment variable with path to apikey.json file
    - export GOOGLE_APPLICATION_CREDENTIALS=/home/<user>/apikey.json
5. Run developer server by command: python3 manage.py runserver